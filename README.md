# Silmar Ng LazyImages

[![npm (scoped)](https://img.shields.io/npm/v/@silmar/ng-lazy-images.svg)](https://www.npmjs.com/package/@silmar/ng-lazy-images)
[![pipeline status](https://gitlab.com/etg-public/silmar-ng-lazy-images/badges/master/pipeline.svg)](https://gitlab.com/etg-public/silmar-ng-lazy-images/commits/master)
[![NPM](https://img.shields.io/npm/l/@silmar/ng-lazy-images.svg?style=flat-square)](https://www.npmjs.com/package/@silmar/ng-lazy-images)

### Install
```
npm i @silmar/ng-lazy-images
// or
yarn add @silmar/ng-lazy-images
```

### Demo
[Checkout the demo page](https://etg-public.gitlab.io/silmar-ng-lazy-images) or run the demo app: check out the project then run the following in th root directory
```
yarn install
ng serve
```

More info in the library [README.md](projects/silmar/ng-lazy-images/README.md)
