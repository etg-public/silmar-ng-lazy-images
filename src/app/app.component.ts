import { Component } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector    : 'app-root',
  templateUrl : './app.component.html',
  styleUrls   : [ './app.component.scss' ]
})
export class AppComponent {
  showLazyImgs = false;
  lazyImgs     = [
    'https://images.unsplash.com/photo-1515783903488-48bee164252b?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1920&q=80',
    'https://images.unsplash.com/photo-1513333420772-7b64ad15ca96?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=934&q=80',
    'https://images.unsplash.com/photo-1533651180995-3b8dcd33e834?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1920&q=80',
    'https://images.unsplash.com/photo-1549977645-49a41e9add3c?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1600&q=80'
  ];

  txt = {
    install : 'yarn add @silmar/ng-lazy-images',
    module  : `import {NgLazyImagesModule} from '@silmar/ng-lazy-images';
// ...
@NgModule({
    declarations: [ AppComponent ],
    imports: [
        //...
        NgLazyImagesModule
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule {}`,
    ex1     : '<img data-src="http://img.photobucket.com/albums/v642/shakespeares_sister/shakes5/squirrel.jpg" siLazyImages/>',
    ex2     : `<div siLazyImages>
    <div>
        <img data-src="https://secure.i.telegraph.co.uk/multimedia/archive/02384/squirrel-michelang_2384189k.jpg"/>
    </div>
    <div><img data-src="https://images.unsplash.com/photo-1509307602489-ee74fee4a97c?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1920&q=80"/>
    </div>
</div>`,
    ex3     : `<div class="columns" siLazyImages>
      <div class="column" *ngFor="let lazy of lazyImgs">
          <img [attr.data-src]="lazy" style="max-height: 300px;"/>
      </div>
</div>`,
    ex4     : `<div class="columns bgr" siLazyImages style="height: 450px;">
    <div class="column"
         data-background-src="https://i.pinimg.com/originals/71/b1/22/71b122da439bf6a5509ef55a75c7c15c.jpg"> </div>
    <div class="column"
         data-background-src="https://i.pinimg.com/originals/2f/e6/32/2fe632acdf7892da603eeb6508dbafdd.jpg"> </div>
</div>`,
    ex5     : `<img siLazyImages siThumbor="expobeds.com/expo_beds_logo.png" [width]="200" [height]="200" [x2]="true"
     [useDataSrc]="true"/>
<si-thumbor-picture siLazyImages path="expobeds.com/expo_beds_logo.png" [x2]="true" [width]="320" [height]="200"
                    [useDataSrc]="true"
                    [bySize]="{'phone':100,'(max-width: 1060px)':'150x100', 'hd':200, 'fullhd': 250}"
                    [set1pxOnLazy]="true"></si-thumbor-picture>`,
    ex6     : `<div class="columns">
  <div class="column">
    <img data-src="https://upload.wikimedia.org/wikipedia/commons/f/fc/American_pika_%28ochotona_princeps%29_with_a_mouthful_of_flowers.jpg"
         style="max-height: 300px;" siLazyImages (loaded)="onImageLoad($event)"/>
  </div>
  <div class="column">
    <img data-src="https://s.hdnux.com/photos/43/41/14/9311035/3/920x920.jpg" siLazyImages
         style="max-height: 300px;" (loading)="onImageLoading($event)"/>
  </div>
</div>`,
    ex7     : `<div siLazyContent (visible)="onDivVisible($event)"></div>`
  };


  constructor(public domS: DomSanitizer) {
    setTimeout(() => this.showLazyImgs = true, 500);
  }

  onImageLoad($e) {
    console.log('Loaded event fired: ', $e);
  }

  onImageLoading($e) {
    console.log('Loading event fired: ', $e);
  }

  onDivVisible($e) {
    alert('Div is about to be visible');
    console.log('Visible event fired: ', $e);
  }
}
