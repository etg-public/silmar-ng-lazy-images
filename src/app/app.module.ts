import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { NgLazyImagesModule } from '../../projects/silmar/ng-lazy-images/src/lib/ng-lazy-images.module';
import { CommonModule } from '@angular/common';
import { ThumborModule } from '@silmar/ng-core/thumbor';

@NgModule({
  declarations : [
    AppComponent
  ],
  imports      : [
    BrowserModule,
    CommonModule,
    ThumborModule.forRoot({
      imagesDomain    : 'ba-dev.etgr.net',
      thumborEndpoint : 'https://jb.etgr.net',
      supportsWebp    : false
    }),
    NgLazyImagesModule
  ],
  providers    : [],
  bootstrap    : [ AppComponent ]
})
export class AppModule {
}
