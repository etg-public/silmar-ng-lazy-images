import { ModuleWithProviders, NgModule } from '@angular/core';
import { NgLazyImagesDirective } from './ng-lazy-images.directive';
import { CommonModule } from "@angular/common";
import { LAZY_IMAGE_CONFIG, LazyConfig, LazyImageConfig } from "./config";
import { NgLazyContentDirective } from './ng-lazy-content.directive';


@NgModule({
  imports      : [ CommonModule ],
  declarations : [ NgLazyImagesDirective, NgLazyContentDirective ],
  exports      : [ NgLazyImagesDirective, NgLazyContentDirective ]
})
export class NgLazyImagesModule {
  static forRoot(lazyConfig?: LazyConfig|LazyImageConfig): ModuleWithProviders<NgLazyImagesModule> {
    return {
      ngModule  : NgLazyImagesModule,
      providers : [ { provide : LAZY_IMAGE_CONFIG, useValue : lazyConfig } ]
    };
  }
}
