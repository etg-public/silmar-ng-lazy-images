import { InjectionToken } from '@angular/core';
import { IntersectionConfigInterface } from './intersection-config.interface';

export const LAZY_IMAGE_CONFIG = new InjectionToken<LazyConfig|LazyImageConfig>('silmar-ng-lazy-module LAZY_IMAGE_CONFIG');

/**
 * @deprecated
 */
export interface LazyImageConfig extends LazyConfig {

}

export interface LazyConfig {
  intersectionConfig?: IntersectionConfigInterface;
  lazyClass?: string;
  loadingClass?: string;
  loadedClass?: string;
  errorClass?: string;
  idleLoadTimeout?: number;
}

export const defaultLazyConfig: LazyConfig = {
  lazyClass          : 'nli',
  loadingClass       : 'nli-loading',
  loadedClass        : 'nli-done',
  errorClass         : 'nli-error',
  idleLoadTimeout    : 100,
  intersectionConfig : {
    threshold  : [ 0.01 ],
    rootMargin : '30px 20px 0px 20px'
  }
};
