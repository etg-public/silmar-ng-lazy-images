import {
  Directive,
  ElementRef, EventEmitter,
  Inject,
  Input,
  NgZone,
  OnDestroy,
  OnInit,
  Optional, Output,
  PLATFORM_ID,
  Renderer2
} from '@angular/core';
import { IntersectionConfigInterface } from './intersection-config.interface';
import { Observable, throwError, forkJoin } from 'rxjs';
import { defaultLazyConfig, LAZY_IMAGE_CONFIG, LazyConfig } from './config';
import { switchMap } from 'rxjs/operators';
import { LazyAbstract } from './lazy.abstract';

/**
 * We do not need ChangeDetectorRef - the Renderer does not need change detection
 */
@Directive({
  selector : '[siLazyImages]'
})
export class NgLazyImagesDirective extends LazyAbstract implements OnInit, OnDestroy {
  /**
   *
   */
  @Input('siLazyClass') lazyCss: string;

  /**
   *
   */
  @Input('siLazyLoadingClass') loadingCss: string;

  /**
   *
   */
  @Input('siLazyLoadedClass') doneCss: string;

  /**
   *
   */
  @Input('siLazyErrorClass') errorCss: string;

  /**
   *
   */
  @Input() loaderSrc: string;

  /**
   *
   */
  @Output() loaded: EventEmitter<{ target: any }> = new EventEmitter();

  /**
   *
   */
  @Output() loading: EventEmitter<{ target: any }> = new EventEmitter();

  /**
   *
   */
  protected domObserver: MutationObserver;

  /**
   * Constructor
   */
  constructor(element: ElementRef, protected render: Renderer2, ngZone: NgZone, @Inject(PLATFORM_ID) platformId: any,
              @Optional() @Inject(LAZY_IMAGE_CONFIG) config: LazyConfig) {
    super(element, ngZone, platformId, config);

    const conf = defaultLazyConfig;

    for (const c in config) {
      if (config.hasOwnProperty(c)) {
        conf[ c ] = config[ c ];
      }
    }

    this.lazyCss    = conf.lazyClass;
    this.loadingCss = conf.loadingClass;
    this.doneCss    = conf.loadedClass;
    this.errorCss   = conf.errorClass;
  }

  /**
   *
   */
  @Input('siLazyImages') set intersectionConfig(config: IntersectionConfigInterface) {
    if (config) {
      this.interConfig = Object.assign({}, this.interConfig, config);
    }
  }

  /**
   * Discover lazy images to loaded/observe
   */
  static getLazyImages(pageNode: HTMLElement) {
    if (pageNode.tagName === 'IMG') {
      return [ pageNode ];
    } else {
      return Array.from(pageNode.querySelectorAll('img[data-src], [data-srcset], [data-background-src]'));
    }
  }

  /**
   *
   */
  protected init() {
    this.removeObserver();

    this.ngZone.runOutsideAngular(() => {
      this.registerObserver();

      this.domObserver = this.observeDOM(() => this.initImages());
      this.domObserver ? this.initImages() : setTimeout(() => this.initImages(), 10);
    });
  }

  /**
   * Gather all lazy images and start observing them
   */
  initImages() {
    const images = NgLazyImagesDirective.getLazyImages(this.root);

    if (!images.length) {
      return;
    }

    if (this.observer) {
      for (const image of images) {
        if (this.lazyCss) {
          this.render.addClass(image, this.lazyCss);
        }

        this.observer.observe(image);
      }
    } else {
      for (const image of images) {
        this.isIntersecting(image);
      }
    }
  }

  observeDOM(onChange: (mutations?) => void) {
    // Create a Mutation Observer instance if browser supports it
    const observer = typeof MutationObserver === 'undefined' ? null : new MutationObserver(mutations => onChange(mutations));

    if (observer) {
      const observerConfig = {
        attributes      : true,
        childList       : this.root.tagName != 'IMG',
        subtree         : this.root.tagName != 'IMG',
        attributeFilter : [ 'data-src', 'data-srcset', 'data-background-src' ]
      };

      // Observe Directive DOM Node
      observer.observe(this.root, observerConfig);
    }

    return observer;
  }

  /**
   * Preload the given image
   */
  loadImage(image) {
    if (!image) {
      return;
    }

    const observables                                              = [];
    const sources: { src?: string, srcset?: string, bgr?: string } = {};
    const loaders: { src?: string, bgr?: string }                  = {};
    let hasSrc, hasLoaderSrc                                       = false;

    const dSet = image.dataset ? image.dataset : {
      backgroundSrc : image.getAttribute('data-background-src'),
      srcset        : image.getAttribute('data-srcset'),
      src           : image.getAttribute('data-src')
    };

    if (dSet) {
      if (dSet.src) {
        sources.src = hasSrc = dSet.src;

        observables.push(this.buffer(dSet.src));

        if (this.loaderSrc) {
          hasLoaderSrc = true;
          loaders.src  = this.loaderSrc;
        }
      }
      if (dSet.srcset) {
        sources.srcset = hasSrc = dSet.srcset;

        if (this.loaderSrc) {
          loaders.src = this.loaderSrc;
        }
      }
      if (dSet.backgroundSrc) {
        sources.bgr = hasSrc = dSet.backgroundSrc;

        if (this.loaderSrc) {
          hasLoaderSrc = true;
          loaders.bgr  = this.loaderSrc;
        }

        observables.push(this.buffer(dSet.backgroundSrc));
      }
    }

    if (hasLoaderSrc) {
      this.setProps(image, loaders);
    }

    // Stop observing the current target
    if (this.observer) {
      this.observer.unobserve(image);
    }

    if (hasSrc) {
      this.removeDataset(image).startsLoading(image);

      const obs = observables.length ?
        forkJoin(observables).pipe(switchMap(() => this.doLoadImage(image, sources))) :
        this.doLoadImage(image, sources);

      obs.subscribe(
        () => this.isDone(image),
        err => {
          if (this.errorCss) {
            this.render.addClass(image, this.errorCss);
          }
        }
      );
    }
  }

  protected isIntersecting(target) {
    this.loadImage(target);
  }

  /**
   * Image was loaded
   */
  protected isDone(image) {
    if (this.loadingCss) {
      this.render.removeClass(image, this.loadingCss);
    }
    if (this.doneCss) {
      this.render.addClass(image, this.doneCss);
    }

    this.ngZone.run(() => {
      this.loaded.emit({ target : image });
    });
  }

  /**
   * Set classes for loading
   */
  protected startsLoading(image) {
    if (this.loadingCss) {
      this.render.addClass(image, this.loadingCss);
    }

    this.ngZone.run(() => {
      this.loading.emit({ target : image });
    });
  }

  /**
   *
   * @param image
   * @param props
   */
  protected doLoadImage(image, props: {}): Observable<any> {
    return new Observable((observer) => {
      image.onload  = () => {
        if (!this.loaderSrc || image.src.indexOf(this.loaderSrc) < 0) {
          observer.next(image);
          observer.complete();
        }
      };
      image.onerror = (err) => throwError(err);

      if (!this.setProps(image, props).async) {
        // The setProps method set background and we cannot know when it will be loaded so fire away
        observer.next(image);
        observer.complete();

        // clear callbacks
        image.onload = image.onerror = () => undefined;
      }
    });
  }

  /**
   *
   */
  protected setProps(image, props: {}): { async: boolean } {
    const raf = window.requestAnimationFrame || ((cb) => setTimeout(cb, 16));

    let async = false;

    for (const prop of Object.keys(props)) {
      let val = props[ prop ];

      if (prop === 'bgr') {
        if (typeof val === 'string' && val.indexOf('\'') > -1) {
          val = val.replace('\'', '\\\'');
        }

        raf(() => this.render.setStyle(image, 'background-image', `url('${val}')`));
      } else {
        async = true;
        raf(() => this.render.setAttribute(image, prop, val));
      }
    }

    return { async };
  }

  /**
   * Fetch the given image URL
   */
  protected buffer(url): Observable<any> {
    return new Observable((observer) => {
      const buffer   = new Image();
      buffer.onload  = () => {
        observer.next(buffer);
        observer.complete();
      };
      buffer.onerror = (err) => throwError(err);
      buffer.src     = url;
    });
  }

  /**
   *
   */
  protected removeDataset(image) {
    for (const prop of [ 'src', 'srcset', 'background-src' ]) {
      this.render.removeAttribute(image, `data-${prop}`);
    }

    return this;
  }

  /**
   * Remove the intersection observer
   */
  protected removeObserver() {
    super.removeObserver();

    if (this.domObserver) {
      this.domObserver.disconnect();
      this.domObserver = undefined;
    }
  }
}
