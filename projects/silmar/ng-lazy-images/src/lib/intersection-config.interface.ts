export interface IntersectionConfigInterface {
  root?: any | null;
  rootMargin?: string;
  threshold?: number | number[];
}