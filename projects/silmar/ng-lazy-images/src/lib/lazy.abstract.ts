import {
  Directive,
  ElementRef,
  Inject,
  NgZone,
  Optional,
  PLATFORM_ID
} from '@angular/core';
import { IntersectionConfigInterface } from './intersection-config.interface';
import { defaultLazyConfig, LAZY_IMAGE_CONFIG, LazyConfig } from './config';
import { isPlatformBrowser } from '@angular/common';

const ric = 'requestIdleCallback';

@Directive()
export abstract class LazyAbstract {
  /**
   * Cache the browser check
   */
  protected isBrowser: boolean;

  /**
   * The root element. This should be parent with images or the image itself
   */
  protected root: any;

  /**
   *
   */
  protected observer: IntersectionObserver;

  /**
   * Configuration object for the intersection observer
   * See here for more info https://developer.mozilla.org/en-US/docs/Web/API/Intersection_Observer_API
   */
  protected interConfig: IntersectionConfigInterface;

  /**
   *
   */
  protected idleTimeout: number;

  /**
   * Constructor
   */
  constructor(element: ElementRef, protected ngZone: NgZone, @Inject(PLATFORM_ID) platformId: any,
              @Optional() @Inject(LAZY_IMAGE_CONFIG) config: LazyConfig) {
    const conf = defaultLazyConfig;

    for (const c in config) {
      if (config.hasOwnProperty(c)) {
        conf[ c ] = config[ c ];
      }
    }

    this.interConfig = conf.intersectionConfig;
    this.idleTimeout = conf.idleLoadTimeout;
    this.isBrowser   = isPlatformBrowser(platformId);
    this.root        = element.nativeElement;
  }

  /**
   * Init the directive, but only if in the browser
   */
  ngOnInit() {
    if (this.isBrowser) {
      this.init();
    }
  }

  /**
   *
   */
  ngOnDestroy() {
    this.removeObserver();
  }

  /**
   * Register the IntersectionObserver if the browser supports it
   */
  registerObserver() {
    if (!('IntersectionObserver' in window)) {
      return false;
    }

    this.observer = new IntersectionObserver(
      entries => this.onIntersection(entries),
      this.interConfig ? this.interConfig : undefined
    );

    return this.observer;
  }

  /**
   * On intersection callback
   *
   */
  protected onIntersection(entries: any[]) {
    for (const entry of entries) {
      if (entry.isIntersecting && entry.intersectionRatio > 0 && entry.target) {
        if (this.idleTimeout && ric in window) {
          window[ ric ](() => this.isIntersecting(entry.target), { timeout : this.idleTimeout });
        } else {
          this.isIntersecting(entry.target);
        }
      }
    }
  }

  /**
   * Remove the intersection observer
   */
  protected removeObserver() {
    if (this.observer) {
      this.observer.disconnect();
      this.observer = undefined;
    }
  }


  protected abstract init();

  protected abstract isIntersecting(target);
}
