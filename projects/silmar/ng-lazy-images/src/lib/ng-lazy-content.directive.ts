import {
  Directive,
  ElementRef,
  EventEmitter,
  Inject, Input,
  NgZone,
  OnDestroy,
  OnInit,
  Optional,
  Output,
  PLATFORM_ID
} from '@angular/core';
import { LazyAbstract } from './lazy.abstract';
import { LAZY_IMAGE_CONFIG, LazyConfig } from './config';
import { IntersectionConfigInterface } from './intersection-config.interface';

@Directive({
  selector : '[siLazyContent]'
})
export class NgLazyContentDirective extends LazyAbstract implements OnInit, OnDestroy {
  @Output() visible: EventEmitter<{ target: any }> = new EventEmitter();

  @Input() once = true;

  /**
   * Constructor
   */
  constructor(element: ElementRef, ngZone: NgZone, @Inject(PLATFORM_ID) platformId: any,
              @Optional() @Inject(LAZY_IMAGE_CONFIG) config: LazyConfig) {
    super(element, ngZone, platformId, config);
  }

  /**
   *
   */
  @Input('siLazyContent') set intersectionConfig(config: IntersectionConfigInterface) {
    if (config) {
      this.interConfig = Object.assign({}, this.interConfig, config);
    }
  }

  protected init() {
    this.removeObserver();

    this.ngZone.runOutsideAngular(() => {
      this.registerObserver();

      if (this.observer) {
        this.observer.observe(this.root);
      } else {
        this.isIntersecting(this.root);
      }
    });
  }

  protected isIntersecting(target) {
    this.ngZone.run(() => {
      this.visible.emit({ target });
    });

    if (this.once) {
      this.removeObserver();
    }
  }
}
